### From http://www.redblobgames.com/pathfinding/a-star/implementation.html for code source and other literature.
# Also see https://pythontips.com/2013/08/07/the-self-variable-in-python-explained/ for class, __init__, and self semiexplained.


import math
import collections
import random
import heapq



# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return math.sqrt(x)
# Redefine pi to something shorter
pi = math.pi
print ''




###### Visuals. What to draw where.

### Create graph with locations (x,y). Find locations inside map, outside impassable terrain, and neighbors that meet those criteria.
class Grid:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.walls = []
        self.water = []
    def in_bounds(self, pos):
        (x, y) = pos
        return 0 <= x < self.width and 0 <= y < self.height
    def passable(self, pos):
        return pos not in self.walls
    def neighbors(self, pos):
        (x, y) = pos
        results = [(x+1, y), (x, y-1), (x-1, y), (x, y+1)]
        if (x + y) % 2 == 0: results.reverse() # aesthetics
        results = filter(self.in_bounds, results)
        results = filter(self.passable, results)
        return results
### Takes above, adds weight
class GridWithWeights(Grid):
    def __init__(self, width, height):
        Grid.__init__(self, width, height)
        self.weights = {}
    def cost(self, from_node, to_node):
        return self.weights.get(to_node, 1)


### Create the graph STYLE for drawing in terminal.
def Tile(graph, tilewidth, pos, tiletype):
    if pos in graph.walls: tile = '#'*tilewidth
    elif 'start' in tiletype and pos == tiletype['start']: tile = 'A'
    elif 'goal'  in tiletype and pos == tiletype['goal']:  tile = 'Z'
    elif 'flow' in tiletype and tiletype['flow'].get(pos, None) is not None:
        (x1, y1) = pos
        (x2, y2) = tiletype['flow'][pos]
        if x2 == x1 + 1: tile = '>'
        if x2 == x1 - 1: tile = '<'
        if y2 == y1 + 1: tile = 'v'
        if y2 == y1 - 1: tile = '^'
    elif 'number' in tiletype and pos in tiletype['number']: tile = '%d' % tiletype['number'][pos]
    elif 'path'   in tiletype and pos in tiletype['path']:   tile = 'o'
    elif pos in graph.water:  tile = '~'*tilewidth
    else: tile = '.'
    return tile


### Draw graph in terminal. Default tilewidth = 2
def DrawGrid(graph, tilewidth=2, **tiletype):
    for y in range(graph.height):
        for x in range(graph.width):
            print '%%-%ds' %tilewidth %Tile(graph, tilewidth, (x,y), tiletype),
            #print Tile(graph, (x,y), tiletype, tilewidth),
        print ''






###### The meaty searching and pathfinding algorithms.

### This 'Queue' class is just a wrapper around the built-in 'collections.deque' class.
class Queue:
    def __init__(self):
        self.elements = collections.deque()
    def empty(self):
        return len(self.elements) == 0
    def put(self, x):
        self.elements.append(x)
    def get(self):
        return self.elements.popleft()
### Above, with priority
class PriorityQueue:
    def __init__(self):
        self.elements = []
    def empty(self):
        return len(self.elements) == 0
    def put(self, item, priority):
        heapq.heappush(self.elements, (priority, item))
    def get(self):
        return heapq.heappop(self.elements)[1]



### Define Breadth-first Search. Only works with string example at bottom of page.
def BreadthFirst1(graph, start):
    frontier = Queue()
    frontier.put(start)
    visited = {}
    visited[start] = True
    while not frontier.empty():
        current = frontier.get()
        print('Visiting %r' % current)
        for neighbor in graph.neighbors(current):
            if neighbor not in visited:
                frontier.put(neighbor)
                visited[neighbor] = True


### To reconstruct paths, need to store location of where we came from, 
# so I've renamed visited (True/False) to camefrom (location).
def BreadthFirst2(graph, start):
    frontier = Queue()
    frontier.put(start)
    camefrom = {}
    camefrom[start] = None
    while not frontier.empty():
        current = frontier.get()
        for neighbor in graph.neighbors(current):
            if neighbor not in camefrom:
                frontier.put(neighbor)
                camefrom[neighbor] = current
    return camefrom


### All we need to do is add an if statement in the main loop.
# This is optional for Breadth First Search or Dijkstra's Algorithm,
# but effectively required for Greedy Best-First Search and A*
def BreadthFirst3(graph, start, goal):
    frontier = Queue()
    frontier.put(start)
    camefrom = {}
    camefrom[start] = None
    i = 0
    while not frontier.empty():
        current = frontier.get()
        if current == goal:
            break
        for neighbor in graph.neighbors(current):
            if neighbor not in camefrom:
                frontier.put(neighbor)
                camefrom[neighbor] = current
        i += 1
    print 'Searches:', i

    return camefrom


### Define Dijkstra Search
def Dijkstra(graph, start, goal):
    frontier = PriorityQueue()
    frontier.put(start, 0)
    camefrom = {}
    costsofar = {}
    camefrom[start] = None
    costsofar[start] = 0
    i = 0
    while not frontier.empty():
        current = frontier.get()
        if current == goal:
            break
        for neighbor in graph.neighbors(current):
            newcost = costsofar[current] + graph.cost(current, neighbor)
            if neighbor not in costsofar or newcost < costsofar[neighbor]:
                costsofar[neighbor] = newcost
                priority = newcost
                frontier.put(neighbor, priority)
                camefrom[neighbor] = current
        i += 1
    print ' Searches:', i
    return camefrom, costsofar



### Both Greedy Best-First Search and A* use a heuristic function. 
# The only difference is that A* uses both the heuristic and the ordering from Dijkstra's Algorithm.
def heuristic(a, b):
    (x1, y1) = a
    (x2, y2) = b
    return abs(x1 - x2) + abs(y1 - y2)

def AStar(graph, start, goal):
    frontier = PriorityQueue()
    frontier.put(start, 0)
    camefrom = {}
    costsofar = {}
    camefrom[start] = None
    costsofar[start] = 0
    i = 0
    while not frontier.empty():
        current = frontier.get()
        if current == goal:
            break
        for neighbor in graph.neighbors(current):
            newcost = costsofar[current] + graph.cost(current, neighbor)
            if neighbor not in costsofar or newcost < costsofar[neighbor]:
                costsofar[neighbor] = newcost
                priority = newcost + heuristic(goal, neighbor)
                frontier.put(neighbor, priority)
                camefrom[neighbor] = current
        i += 1
    print ' Searches:', i
    return camefrom, costsofar




### Finally, after searching I need to build the path.
def ReconstructPath(camefrom, start, goal):
    current = goal
    path = [current]
    while current != start:
        current = camefrom[current]
        path.append(current)
    path.append(start) # optional
    path.reverse() # optional
    print ' Path length:', len(path)-3 #-1, then -2 because it also counts A,Z as spots.
    return path










TileWidth = 1
GridWidth = 30
GridHeight = 15 #20

#Walls2 = [(i%GridWidth, i//GridWidth) for i in [21,22,51,52,81,82,93,94,111,112,123,124,133,134,141,142,153,154,163,164,171,172,173,174,175,183,184,193,194,201,202,203,204,205,213,214,223,224,243,244,253,254,273,274,283,284,303,304,313,314,333,334,343,344,373,374,403,404,433,434]]
Walls2 = [(21,0),(22,0),(21,1),(22,1),(21,2),(22,2),(3,3),(4,3),(21,3),(22,3),(3,4),(4,4),(13,4),(14,4),(21,4),(22,4),(3,5),(4,5),(13,5),(14,5),(21,5),(22,5),(23,5),(24,5),(25,5),(3,6),(4,6),(13,6),(14,6),(21,6),(22,6),(23,6),(24,6),(25,6),(3,7),(4,7),(13,7),(14,7),(3,8),(4,8),(13,8),(14,8),(3,9),(4,9),(13,9),(14,9),(3,10),(4,10),(13,10),(14,10),(3,11),(4,11),(13,11),(14,11),(13,12),(14,12),(13,13),(14,13),(13,14),(14,14)]

nWalls = int(GridWidth*GridHeight/3)
Walls3 = [(random.randint(0,GridWidth),random.randint(0,GridHeight)) for i in range(nWalls)]



nWalls = 6
rWalls = []
for n in range(nWalls/3+1):
  #if random.random < 0.5:
  xs = (random.randint(0,GridWidth/4), random.randint(0,GridWidth))
  x1 = min(xs)
  x2 = max(xs)
  y = random.randint(0,GridHeight)
  rWalls.extend([(x,y) for x in range(x1,x2)])
  #'''
  #else:
  ys = (random.randint(0,GridHeight/4), random.randint(0,GridHeight))
  y1 = min(ys)
  y2 = max(ys)
  x = random.randint(0,GridWidth)
  rWalls.extend([(x,y) for y in range(y1,y2)])
  #'''

'''
Wall_a = [(2,2+y) for y in range(10)]
Wall_b = [(10,4+y) for y in range(10)]
Wall_c = [(x,2) for x in range(10)]
Wall_d = [(x+10,10) for x in range(10)]

Wallz = Wall_a + Wall_b + Wall_c + Wall_d
'''


### Create only straight-line walls
# [(,),(,)]
walls = [ [(3,1),(3,14)], 
          [(1,1),(24,1)], 
          [(10,3),(10,15)], 
          [(23,0),(23,1)],
          [(15,10),(25,10)],
          #[(15,2),(15,10)]
 ]
Walls = []
for i in walls:
  x1 = i[0][0]
  y1 = i[0][1]
  x2 = i[1][0]
  y2 = i[1][1]
  #Walls.extend( [(x,y) for x,y in zip(range(x1,x2),range(y1,y2))] )
  #mm = max(max(i))
  if x2-x1 == 0: # (x1 = x2)
    Walls.extend( [(x1,y) for y in range(y1,y2)] )
  if y2-y1 == 0: # (y1 = y2)
    Walls.extend( [(x,y1) for x in range(x1,x2)] )



### Create only straight-line water
# [(,),(,)]
waterweight = 10
water = [ [(15,2),(15,10)],
          [(16,2),(16,10)],
 ]
Water = []
for i in water:
  x1 = i[0][0]
  y1 = i[0][1]
  x2 = i[1][0]
  y2 = i[1][1]
  if x2-x1 == 0: # (x1 = x2)
    Water.extend( [(x1,y) for y in range(y1,y2)] )
  if y2-y1 == 0: # (y1 = y2)
    Water.extend( [(x,y1) for x in range(x1,x2)] )








### Draw grid, no searches
g = Grid(GridWidth, GridHeight)
g.walls = Walls #rWalls #Wallz #Walls2 #Walls3
g.water = Water
#DrawGrid(g, TileWidth)
print ''




### Draw Breadth First Search
startx = 2
starty = 7
#CameFrom = BreadthFirst2(g, (startx, starty))
#DrawGrid(g, TileWidth, flow=CameFrom, start=(startx, starty))
#print ''

goalx = 20
goaly = 8
print ''
print 'Breadth First'
CameFrom = BreadthFirst3(g, (startx, starty), (goalx, goaly))
print ' Flow, Z-A'
DrawGrid(g, TileWidth, flow=CameFrom, start=(startx, starty), goal=(goalx, goaly))
#print ''
print ' Path'
DrawGrid(g, TileWidth, path=ReconstructPath(CameFrom, start=(startx, starty), goal=(goalx, goaly)))




'''
startx = 1
starty = 4
goalx = 7
goaly = 8

#gw = GridWithWeights(GridWidth, GridHeight)
#gw.walls = Walls2
gw = GridWithWeights(10, 10)
gw.walls = [(1, 7), (1, 8), (2, 7), (2, 8), (3, 7), (3, 8)]
gw.weights = {pos: 5 for pos in [(3, 4), (3, 5), (4, 1), (4, 2),
                                       (4, 3), (4, 4), (4, 5), (4, 6),
                                       (4, 7), (4, 8), (5, 1), (5, 2),
                                       (5, 3), (5, 4), (5, 5), (5, 6),
                                       (5, 7), (5, 8), (6, 2), (6, 3),
                                       (6, 4), (6, 5), (6, 6), (6, 7),
                                       (7, 3), (7, 4), (7, 5)]}
print ''
DrawGrid(gw, TileWidth)
'''
gw = GridWithWeights(GridWidth, GridHeight)
gw.walls = Walls
gw.water = Water
gw.weights = {pos: waterweight for pos in Water}


### Dijkstra Search
print ''
print ''
print 'Dijkstra'
print ' Flow, Z-A' 
CameFrom, CostSoFar = Dijkstra(gw, (startx, starty), (goalx, goaly))
DrawGrid(gw, TileWidth, flow=CameFrom, start=(startx, starty), goal=(goalx, goaly))
#print''
#print 'Dijkstra, cost:'
#DrawGrid(gw, TileWidth, number=CostSoFar, start=(startx, starty), goal=(goalx, goaly))
#print ''
print ' Path'
DrawGrid(gw, TileWidth, path=ReconstructPath(CameFrom, start=(startx, starty), goal=(goalx, goaly)))
print ''


### A* Search
print ''
print 'A*'
print ' Flow, Z-A'
CameFrom, CostSoFar = AStar(gw, (startx, starty), (goalx, goaly))
DrawGrid(gw, TileWidth, flow=CameFrom, start=(startx, starty), goal=(goalx, goaly))
#print''
#print 'A*, cost:'
#DrawGrid(gw, TileWidth, number=CostSoFar, start=(startx, starty), goal=(goalx, goaly))
#print ''
print ' Path'
DrawGrid(gw, TileWidth, path=ReconstructPath(CameFrom, start=(startx, starty), goal=(goalx, goaly)))
print ''




















### Short example with a graph space consisting of letter nodes and uni- or bidirectional edges, or vectors, joining them.
'''
class SimpleGraph:
    def __init__(self):
        self.edges = {}

    def neighbors(self, id):
        return self.edges[id]

example_graph = SimpleGraph()
example_graph.edges = {'A': ['B'],
                       'B': ['A', 'C', 'D'],
                       'C': ['A'],
                       'D': ['E', 'A'],
                       'E': ['B']}

BreadthFirst_1(example_graph, 'A')
'''
