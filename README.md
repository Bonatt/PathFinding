### Exploring pathfinding algorithms with the help of [Red Blob Games](https://www.redblobgames.com/)

Red Blob Games is probably the most helpful source I've ever discovered for game algorithms. 
Amil has many articles outlining both theory and implementation (with a scope on RPGs) that include great interactive visualizations.
I decided to follow his [A* guide](https://www.redblobgames.com/pathfinding/a-star/introduction.html).

While most of the bones of my code follows Amil closely, the spirit and syntax of this implementation remain my own. 
I've documented where appropriate and expanded upon his foundation. 
Please see [implementation.py](https://gitlab.com/Bonatt/PathFinding/blob/master/Old/implementation.py) for his 
and [PathFinding.py](https://gitlab.com/Bonatt/PathFinding/blob/master/PathFinding.py) for mine, here.
[heapq](https://docs.python.org/2/library/heapq.html) is the powerhouse of these algorithms.

Below I have generated a map space with some custom terrain. All pathfinding algorithms employ 
some cost of travel. Travelling through low-weight terrain is low-cost ("quick") and travelling through 
high-weight terrain is high-cost ("slow").
The aim is reduce the total cost of the path by "minimizing" some function of distance and terrain weight. 
This finds the "best" path.

```
A = start location
Z = finish location
. = ground, weight 1
## = walls, weight infinity
~~ = water, weight 10
"" = weeds, weight 5
```
```
.  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
.  ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## .
.  .  .  ## .  .  .  .  .  .  .  .  .  .  .  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## .  ## ## ## ## ## ## .  .  .  .  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## .  .  .  .  .  .  ## .  .  .  .  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## .  .  .  .  .  .  ## ## ## ## .  ~~ ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## .  .  .  .  .  .  .  .  .  ## .  ~~ ~~ ~~ ~~ .  .  .  .  "" .  .  .  .  ## .
.  A  .  ## .  .  .  .  .  .  .  .  .  ## .  ~~ ~~ ~~ ~~ .  .  .  "" "" "" .  .  .  ## .
.  .  .  ## .  .  .  .  .  .  .  .  .  ## .  ~~ ~~ ~~ ~~ .  Z  .  "" "" "" .  .  .  ## .
.  .  .  ## .  .  .  .  .  .  .  .  .  ## .  .  ~~ ~~ ~~ .  .  .  "" "" "" "" "" .  ## .
.  .  .  ## .  .  .  .  .  .  ## .  .  ## .  ## ## ## ## ## ## ## ## ## ## .  .  .  ## .
.  .  .  ## .  .  .  .  .  .  ## .  .  ## .  .  .  .  .  .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## .  .  .  .  .  .  ## .  .  ## .  .  .  .  .  .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## ## ## ## ## .  .  ## .  .  ## .  .  .  .  .  .  .  .  .  .  .  ## ## ## ## .
.  .  .  .  .  .  .  .  .  .  ## ## ## ## .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
```

Here I'll discuss four pathfinding algorithms: 
[breadth-first](https://en.wikipedia.org/wiki/Breadth-first_search), 
[greedy best-first](https://en.wikipedia.org/wiki/Best-first_search),
[Dijkstra](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm), and
[A*](https://en.wikipedia.org/wiki/A*_search_algorithm).

### BREADTH FIRST
Breadth-first explores equally in all directions and does not account for terrain weight.
When the frontier (outer-most branch) finds Z, the search stops and the shortest distance back to A is saved. 
This is an early exit, and all of the following algorithms employ it.
I've represented the search space _back_ from Z to the origin A as arrows, and then the shortest path with xs.
Notice how except for walls, pathing competely ignores the cost of the water terrain. 
This algorithm is useful for easily finding the shortest traveable _distance_ to some location.
Pathing here is only cardinal -- no diagonals; that could be an easy fix later.

```
 Searches: 260
v  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <
v  ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ^
v  v  v  ## v  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  <  .  ## ^
v  v  v  ## v  ## ## ## ## ## ## ^  ^  <  <  <  <  <  <  <  <  <  <  <  <  <  .  .  ## ^
v  v  v  ## v  v  v  v  v  v  ## ^  ^  ^  <  <  <  <  <  <  <  <  <  <  <  .  .  .  ## ^
v  v  v  ## v  v  v  v  v  v  ## ## ## ## ^  <  <  <  <  <  <  <  <  <  .  .  .  .  ## ^
v  v  <  ## v  v  v  v  v  v  v  v  <  ## ^  ^  <  <  <  <  <  <  <  "" .  .  .  .  ## ^
>  A  <  ## v  v  v  v  v  v  v  <  <  ## ^  ^  ^  <  <  <  <  <  "" "" "" .  .  .  ## ^
>  ^  <  ## v  v  v  v  v  v  <  <  <  ## ^  ^  ^  ^  <  <  Z  .  "" "" "" .  .  .  ## ^
^  ^  ^  ## >  v  v  v  v  v  <  <  <  ## ^  ^  ^  ^  ^  <  <  .  "" "" "" "" "" .  ## ^
^  ^  ^  ## >  >  v  v  v  v  ## ^  <  ## ^  ## ## ## ## ## ## ## ## ## ## .  .  .  ## ^
^  ^  ^  ## >  >  >  v  v  v  ## ^  ^  ## ^  <  <  <  .  .  .  .  .  .  .  .  .  .  ## ^
^  ^  ^  ## >  >  >  >  v  v  ## ^  ^  ## ^  ^  <  .  .  .  .  .  .  .  .  .  .  .  ## ^
^  ^  ^  ## ## ## ## ## v  <  ## ^  ^  ## ^  ^  .  .  .  .  .  .  .  .  .  ## ## ## ## ^
^  ^  ^  <  <  <  <  <  <  <  ## ## ## ## ^  .  .  .  .  .  .  .  .  .  .  .  .  .  >  ^
 Path length: 51
.  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
.  ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## .
.  .  .  ## x  x  x  x  x  x  x  x  x  .  .  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## x  ## ## ## ## ## ## .  x  x  .  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## x  .  .  .  .  .  ## .  .  x  x  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## x  .  .  .  .  .  ## ## ## ## x  x  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## x  .  .  .  .  .  .  .  .  ## .  x  x  ~~ ~~ .  .  .  .  "" .  .  .  .  ## .
.  A  .  ## x  .  .  .  .  .  .  .  .  ## .  ~~ x  x  ~~ .  .  .  "" "" "" .  .  .  ## .
.  x  x  ## x  .  .  .  .  .  .  .  .  ## .  ~~ ~~ x  x  x  Z  .  "" "" "" .  .  .  ## .
.  .  x  ## x  x  .  .  .  .  .  .  .  ## .  .  ~~ ~~ ~~ .  .  .  "" "" "" "" "" .  ## .
.  .  x  ## .  x  x  .  .  .  ## .  .  ## .  ## ## ## ## ## ## ## ## ## ## .  .  .  ## .
.  .  x  ## .  .  x  x  .  .  ## .  .  ## .  .  .  .  .  .  .  .  .  .  .  .  .  .  ## .
.  .  x  ## .  .  .  x  x  .  ## .  .  ## .  .  .  .  .  .  .  .  .  .  .  .  .  .  ## .
.  .  x  ## ## ## ## ## x  .  ## .  .  ## .  .  .  .  .  .  .  .  .  .  .  ## ## ## ## .
.  .  x  x  x  x  x  x  x  .  ## ## ## ## .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
```


### DIJKSTRA

Dijkstra is kind of like breadth-first but takes into account terrain weights. 
And thus instead of exploring equally in all directions it can prioritize lower-cost paths.
The total cost from A to any location on the map is shown first, then the lowest-cost path.
The problem with this, however, is that this algorithm wastes computational time exploring 
in directions that are not promising.

Notice how this algorithm doesn't "stair" through a diagonal but instead takes long, straight lines.
Again, incorporating diagonals would greatly improve the result.

```
 Searches: 342
8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37
7  ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 38
6  5  6  ## 30 31 32 33 34 35 36 37 38 39 40 50 60 70 71 72 73 74 75 75 74 73 72 71 ## 39
5  4  5  ## 29 ## ## ## ## ## ## 38 39 40 41 51 61 71 72 73 74 75 75 74 73 72 71 70 ## 40
4  3  4  ## 28 27 26 25 24 25 ## 39 40 41 42 52 62 72 73 74 75 75 74 73 72 71 70 69 ## 41
3  2  3  ## 27 26 25 24 23 24 ## ## ## ## 43 53 63 73 83 75 75 74 73 72 71 70 69 68 ## 42
2  1  2  ## 26 25 24 23 22 23 24 25 26 ## 44 54 64 74 84 76 76 75 74 75 70 69 68 67 ## 43
1  A  1  ## 25 24 23 22 21 22 23 24 25 ## 45 55 65 75 85 77 77 76 79 78 73 68 67 66 ## 44
2  1  2  ## 24 23 22 21 20 21 22 23 24 ## 46 56 66 76 86 78 Z  77 82 77 72 67 66 65 ## 45
3  2  3  ## 23 22 21 20 19 20 21 22 23 ## 47 48 58 68 78 79 .  78 81 76 71 66 67 64 ## 46
4  3  4  ## 22 21 20 19 18 19 ## 23 24 ## 48 ## ## ## ## ## ## ## ## ## ## 61 62 63 ## 47
5  4  5  ## 21 20 19 18 17 18 ## 24 25 ## 49 50 51 52 53 54 55 56 57 58 59 60 61 62 ## 48
6  5  6  ## 20 19 18 17 16 17 ## 25 26 ## 50 51 52 53 54 55 56 57 58 59 58 59 60 61 ## 49
7  6  7  ## ## ## ## ## 15 16 ## 26 27 ## 51 52 53 54 55 56 57 58 59 58 57 ## ## ## ## 50
8  7  8  9  10 11 12 13 14 15 ## ## ## ## 52 53 54 55 56 57 58 59 58 57 56 55 54 53 52 51
 Path length: 73
.  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
.  ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## .
.  .  .  ## x  x  x  x  x  x  x  x  .  .  .  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## x  ## ## ## ## ## ## x  .  .  .  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## x  .  .  .  .  .  ## x  x  x  x  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## x  .  .  .  .  .  ## ## ## ## x  ~~ ~~ ~~ ~~ .  x  x  x  x  x  .  .  .  ## .
.  .  .  ## x  .  .  .  .  .  .  .  .  ## x  ~~ ~~ ~~ ~~ .  x  .  .  "" x  x  .  .  ## .
.  A  .  ## x  .  .  .  .  .  .  .  .  ## x  ~~ ~~ ~~ ~~ .  x  .  "" "" "" x  .  .  ## .
.  x  .  ## x  .  .  .  .  .  .  .  .  ## x  ~~ ~~ ~~ ~~ .  Z  .  "" "" "" x  .  .  ## .
.  x  .  ## x  .  .  .  .  .  .  .  .  ## x  .  ~~ ~~ ~~ .  .  .  "" "" "" x  "" .  ## .
.  x  .  ## x  .  .  .  .  .  ## .  .  ## x  ## ## ## ## ## ## ## ## ## ## x  .  .  ## .
.  x  .  ## x  .  .  .  .  .  ## .  .  ## x  x  x  x  x  x  x  x  x  x  x  x  .  .  ## .
.  x  .  ## x  x  x  x  x  .  ## .  .  ## .  .  .  .  .  .  .  .  .  .  .  .  .  .  ## .
.  x  .  ## ## ## ## ## x  .  ## .  .  ## .  .  .  .  .  .  .  .  .  .  .  ## ## ## ## .
.  x  x  x  x  x  x  x  x  .  ## ## ## ## .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
```


### GREEDY BEST-FIRST

Greed best-first employs a [heuristic function](https://en.wikipedia.org/wiki/Heuristic_(computer_science)).
Since we are only trying to find the path to a single location (the above finds paths to ~all locations and then
takes the shortest path to the desired location), we can apply a heuristic to prioritize paths that reduce 
the distance (or cost) to desired location.
The problem with this, however, is that the blazed path may not actually be the quickest path: 
like breadth-first, it does not account for terrain weights.

```
 Searches: 139
.  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
.  ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## .
.  v  v  ## v  <  <  <  <  <  <  <  <  .  .  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
v  v  v  ## v  ## ## ## ## ## ## ^  <  v  v  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
v  v  v  ## v  v  v  v  v  v  ## ^  <  <  <  <  ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
v  v  v  ## v  v  v  v  v  v  ## ## ## ## ^  <  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  ## .
v  v  v  ## v  v  v  v  v  v  v  v  v  ## ^  <  ~~ ~~ ~~ .  .  .  .  "" .  .  .  .  ## .
>  A  <  ## v  v  v  v  v  v  v  v  v  ## ^  <  v  v  v  v  .  .  "" "" "" .  .  .  ## .
>  ^  <  ## >  >  >  >  v  <  <  <  <  ## ^  <  <  <  <  <  Z  .  "" "" "" .  .  .  ## .
^  ^  ^  ## ^  ^  ^  >  v  <  ^  ^  ^  ## ^  ^  ^  ^  ^  ^  .  .  "" "" "" "" "" .  ## .
^  ^  ^  ## ^  ^  ^  >  v  <  ## ^  ^  ## .  ## ## ## ## ## ## ## ## ## ## .  .  .  ## .
^  ^  ^  ## ^  ^  ^  >  v  <  ## ^  ^  ## .  .  .  .  .  .  .  .  .  .  .  .  .  .  ## .
^  ^  ^  ## ^  ^  ^  >  v  <  ## ^  ^  ## .  .  .  .  .  .  .  .  .  .  .  .  .  .  ## .
^  ^  ^  ## ## ## ## ## v  <  ## ^  ^  ## .  .  .  .  .  .  .  .  .  .  .  ## ## ## ## .
.  ^  ^  <  <  <  <  <  <  <  ## ## ## ## .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
 Path length: 51
.  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
.  ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## .
.  .  .  ## x  x  x  x  x  x  x  x  .  .  .  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## x  ## ## ## ## ## ## x  .  .  .  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## x  .  .  .  .  .  ## x  x  x  x  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## x  .  .  .  .  .  ## ## ## ## x  ~~ ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## x  .  .  .  .  .  .  .  .  ## x  ~~ ~~ ~~ ~~ .  .  .  .  "" .  .  .  .  ## .
.  A  .  ## x  .  .  .  .  .  .  .  .  ## x  ~~ ~~ ~~ ~~ .  .  .  "" "" "" .  .  .  ## .
.  x  x  ## x  x  x  x  x  .  .  .  .  ## x  x  x  x  x  x  Z  .  "" "" "" .  .  .  ## .
.  .  x  ## .  .  .  .  x  .  .  .  .  ## .  .  ~~ ~~ ~~ .  .  .  "" "" "" "" "" .  ## .
.  .  x  ## .  .  .  .  x  .  ## .  .  ## .  ## ## ## ## ## ## ## ## ## ## .  .  .  ## .
.  .  x  ## .  .  .  .  x  .  ## .  .  ## .  .  .  .  .  .  .  .  .  .  .  .  .  .  ## .
.  .  x  ## .  .  .  .  x  .  ## .  .  ## .  .  .  .  .  .  .  .  .  .  .  .  .  .  ## .
.  .  x  ## ## ## ## ## x  .  ## .  .  ## .  .  .  .  .  .  .  .  .  .  .  ## ## ## ## .
.  .  x  x  x  x  x  x  x  .  ## ## ## ## .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
```


### A STAR

A* is the best of Dijkstra and greedy best-first. It effectively sums the distance from the starting location (as in Dijkstra)
and the distance from desired location (as in greedy best-first). Minimizing these together prioritizes promising paths 
but does not waste computational time searching the entire map.

```
 Searches: 293
8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37
7  ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 38
6  5  6  ## 30 31 32 33 34 35 36 37 38 39 40 50 60 70 .  .  .  .  .  .  .  .  .  .  ## 39
5  4  5  ## 29 ## ## ## ## ## ## 38 39 40 41 51 61 71 .  .  .  .  .  .  .  .  .  .  ## 40
4  3  4  ## 28 27 26 25 24 25 ## 39 40 41 42 52 62 72 .  .  76 75 74 73 72 .  .  .  ## 41
3  2  3  ## 27 26 25 24 23 24 ## ## ## ## 43 53 63 73 ~~ 76 75 74 73 72 71 70 69 68 ## 42
2  1  2  ## 26 25 24 23 22 23 24 25 26 ## 44 54 64 74 ~~ 77 76 75 74 75 70 69 68 67 ## 43
1  A  1  ## 25 24 23 22 21 22 23 24 25 ## 45 55 65 75 ~~ 78 77 78 "" "" 73 68 67 66 ## 44
2  1  2  ## 24 23 22 21 20 21 22 23 24 ## 46 56 66 76 ~~ .  Z  .  "" 77 72 67 66 65 ## 45
3  2  3  ## 23 22 21 20 19 20 21 22 23 ## 47 48 58 68 78 .  .  .  "" 76 71 66 67 64 ## 46
4  3  4  ## 22 21 20 19 18 19 ## 23 24 ## 48 ## ## ## ## ## ## ## ## ## ## 61 62 63 ## 47
5  4  5  ## 21 20 19 18 17 18 ## 24 25 ## 49 50 51 52 53 54 55 56 57 58 59 60 61 62 ## 48
6  5  6  ## 20 19 18 17 16 17 ## 25 26 ## 50 51 52 53 54 55 56 57 58 59 58 59 60 61 ## 49
7  6  7  ## ## ## ## ## 15 16 ## 26 27 ## 51 52 53 54 55 56 57 58 59 58 57 ## ## ## ## 50
8  7  8  9  10 11 12 13 14 15 ## ## ## ## 52 53 54 55 56 57 58 59 58 57 56 55 54 53 52 51
 Path length: 73
.  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
.  ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## .
.  .  .  ## x  x  x  x  x  x  x  x  .  .  .  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## x  ## ## ## ## ## ## x  .  .  .  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## x  .  .  .  .  .  ## x  x  x  x  ~~ ~~ ~~ .  .  .  .  .  .  .  .  .  .  ## .
.  .  .  ## x  .  .  .  .  .  ## ## ## ## x  ~~ ~~ ~~ ~~ .  x  x  x  x  x  .  .  .  ## .
.  .  .  ## x  .  .  .  .  .  .  .  .  ## x  ~~ ~~ ~~ ~~ .  x  .  .  "" x  x  .  .  ## .
.  A  .  ## x  .  .  .  .  .  .  .  .  ## x  ~~ ~~ ~~ ~~ .  x  .  "" "" "" x  .  .  ## .
.  x  x  ## x  x  x  x  x  .  .  .  .  ## x  ~~ ~~ ~~ ~~ .  Z  .  "" "" "" x  .  .  ## .
.  .  x  ## .  .  .  .  x  .  .  .  .  ## x  .  ~~ ~~ ~~ .  .  .  "" "" "" x  "" .  ## .
.  .  x  ## .  .  .  .  x  .  ## .  .  ## x  ## ## ## ## ## ## ## ## ## ## x  .  .  ## .
.  .  x  ## .  .  .  .  x  .  ## .  .  ## x  x  x  x  x  x  x  x  x  x  x  x  .  .  ## .
.  .  x  ## .  .  .  .  x  .  ## .  .  ## .  .  .  .  .  .  .  .  .  .  .  .  .  .  ## .
.  .  x  ## ## ## ## ## x  .  ## .  .  ## .  .  .  .  .  .  .  .  .  .  .  ## ## ## ## .
.  .  x  x  x  x  x  x  x  .  ## ## ## ## .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
```

### Conclusion

| | Breadth-first | Dijkstra | Greedy best-first | A* |
| --- | --- | --- | --- | --- |
| Searches | 260 | 342 | 139 | 293 |
| Path length | 51 | 73 | 51 | 73 |

If all terrain is equally weighted, greedy best-first search is the most efficient algorithm if accuracy is not demanded.
And if all terrain is not weighted equally, A* is the most efficient algorithm.
