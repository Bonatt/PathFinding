### From http://www.redblobgames.com/pathfinding/a-star/implementation.html for code source and other literature.
# Also see https://pythontips.com/2013/08/07/the-self-variable-in-python-explained/ for class, __init__, and self semiexplained.


import math
import collections
import random
import heapq



# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return math.sqrt(x)
# Redefine pi to something shorter
pi = math.pi
print ''




###### Visuals. What to draw where.

### Create graph with locations (x,y). Find locations inside map, outside impassable terrain, and neighbors that meet those criteria.
class Grid:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.walls = []
        self.water = []
        self.weeds = []
    def in_bounds(self, pos):
        (x, y) = pos
        return 0 <= x < self.width and 0 <= y < self.height
    def passable(self, pos):
        return pos not in self.walls
    def neighbors(self, pos):
        (x, y) = pos
        # Square
        results = [(x+1, y), (x, y-1), (x-1, y), (x, y+1)]
        if (x + y) % 2 == 0: results.reverse() # Fakes diagonals by making "stairs"
        # Diagonal
        #results = [(x+1, y), (x, y-1), (x-1, y), (x, y+1),  (x+1,y+1),(x+1,y-1),(x-1,y+1),(x-1,y-1)]
        results = filter(self.in_bounds, results)
        results = filter(self.passable, results)
        return results

### Takes above, adds weight
class GridWithWeights(Grid):
    def __init__(self, width, height):
        Grid.__init__(self, width, height)
        self.weights = {}
    def cost(self, from_node, to_node):
        return self.weights.get(to_node, 1)


### Create the graph STYLE for drawing in terminal.
def Tile(graph, tilewidth, pos, tiletype):
    if pos in graph.walls: tile = '#'*tilewidth
    elif 'start' in tiletype and pos == tiletype['start']: tile = 'A'
    elif 'goal'  in tiletype and pos == tiletype['goal']:  tile = 'Z'
    elif 'flow' in tiletype and tiletype['flow'].get(pos, None) is not None:
        (x1, y1) = pos
        (x2, y2) = tiletype['flow'][pos]
        # Square
        #'''
        if x2 == x1 + 1: tile = '>'
        if x2 == x1 - 1: tile = '<'
        if y2 == y1 + 1: tile = 'v'
        if y2 == y1 - 1: tile = '^'
        #'''
        # Diagonal
        '''
        if x2 == x1 + 1: tile = '>'
        elif x2 == x1 - 1: tile = '<'
        elif y2 == y1 + 1: tile = 'v'
        elif y2 == y1 - 1: tile = '^'
        '''
    elif 'number' in tiletype and pos in tiletype['number']: tile = '%d' % tiletype['number'][pos]
    elif 'path'   in tiletype and pos in tiletype['path']:   tile = 'x'
    elif pos in graph.water:  tile = '~'*tilewidth
    elif pos in graph.weeds:  tile = '"'*tilewidth
    else: tile = '.'
    return tile


### Draw graph in terminal. Default tilewidth = 2
def DrawGrid(graph, tilewidth=2, **tiletype):
    for y in range(graph.height):
        #print ''
        for x in range(graph.width):
            print '%%-%ds' %tilewidth %Tile(graph, tilewidth, (x,y), tiletype),
        print ''






###### The meaty searching and pathfinding algorithms.

### This 'Queue' class is just a wrapper around the built-in 'collections.deque' class.
class Queue:
    def __init__(self):
        self.elements = collections.deque()
    def empty(self):
        return len(self.elements) == 0
    def put(self, x):
        self.elements.append(x)
    def get(self):
        return self.elements.popleft()

### Above, with priority
class PriorityQueue:
    def __init__(self):
        self.elements = []
    def empty(self):
        return len(self.elements) == 0
    def put(self, item, priority):
        heapq.heappush(self.elements, (priority, item))
    def get(self):
        return heapq.heappop(self.elements)[1]


### Define Breadth-first Search. Only works with string example at bottom of page.
# Note: that string was deleted. See source.
'''
def BreadthFirst1(graph, start):
    frontier = Queue()
    frontier.put(start)
    visited = {}
    visited[start] = True
    while not frontier.empty():
        current = frontier.get()
        print('Visiting %r' % current)
        for neighbor in graph.neighbors(current):
            if neighbor not in visited:
                frontier.put(neighbor)
                visited[neighbor] = True
'''

### To reconstruct paths, need to store location of where we came from, so I've renamed visited (True/False) to camefrom (location).
'''
def BreadthFirst2(graph, start):
    frontier = Queue()
    frontier.put(start)
    camefrom = {}
    camefrom[start] = None
    while not frontier.empty():
        current = frontier.get()
        for neighbor in graph.neighbors(current):
            if neighbor not in camefrom:
                frontier.put(neighbor)
                camefrom[neighbor] = current
    return camefrom
'''

### All we need to do is add an if statement in the main loop [to stop when reach goal].
# This is optional for Breadth First Search or Dijkstra's Algorithm, but effectively required for Greedy Best-First Search and A*.
def BreadthFirst3(graph, start, goal):
    frontier = Queue()
    frontier.put(start)
    camefrom = {}
    camefrom[start] = None
    i = 0
    while not frontier.empty():
        current = frontier.get()
        if current == goal:
            break
        for neighbor in graph.neighbors(current):
            if neighbor not in camefrom:
                frontier.put(neighbor)
                camefrom[neighbor] = current
        i += 1
    print ' Searches:', i
    return camefrom


### Define Dijkstra Search (also called Uniform Cost Search). 
# We'd like the pathfinder to take movement costs (water, weeds) into account.
def Dijkstra(graph, start, goal):
    frontier = PriorityQueue()
    frontier.put(start, 0)
    camefrom = {}
    costsofar = {}
    camefrom[start] = None
    costsofar[start] = 0
    i = 0
    while not frontier.empty():
        current = frontier.get()
        if current == goal:
            break
        for neighbor in graph.neighbors(current):
            newcost = costsofar[current] + graph.cost(current, neighbor)
            if neighbor not in costsofar or newcost < costsofar[neighbor]:
                costsofar[neighbor] = newcost
                priority = newcost
                frontier.put(neighbor, priority)
                camefrom[neighbor] = current
        i += 1
    print ' Searches:', i
    return camefrom, costsofar


### Both Greedy Best-first Search and A* use a heuristic function. 
# The only difference is that A* uses both the heuristic and the ordering from Dijkstra's Algorithm.
def heuristic(a, b):
    (x1, y1) = a
    (x2, y2) = b
    return abs(x1 - x2) + abs(y1 - y2)

### Define Greed Best-First Search
def GreedyFirst(graph, start, goal):
    frontier = PriorityQueue()
    frontier.put(start, 0)
    camefrom = {}
    camefrom[start] = None
    i = 0
    while not frontier.empty():
        current = frontier.get()
        if current == goal:
            break
        for neighbor in graph.neighbors(current):
            if neighbor not in camefrom:
                priority = heuristic(neighbor, goal)
                frontier.put(neighbor, priority)
                camefrom[neighbor] = current
        i += 1
    print ' Searches:', i
    return camefrom

### Dijkstra's Algorithm works well to find the shortest path, but it wastes time exploring in directions that aren't promising. 
# Greedy Best-first Search explores in promising directions but it may not find the shortest path. 
# The A* algorithm uses both the actual distance from the start and the estimated distance to the goal.
def AStar(graph, start, goal):
    frontier = PriorityQueue()
    frontier.put(start, 0)
    camefrom = {}
    costsofar = {}
    camefrom[start] = None
    costsofar[start] = 0
    i = 0
    while not frontier.empty():
        current = frontier.get()
        if current == goal:
            break
        for neighbor in graph.neighbors(current):
            newcost = costsofar[current] + graph.cost(current, neighbor)
            if neighbor not in costsofar or newcost < costsofar[neighbor]:
                costsofar[neighbor] = newcost
                priority = newcost + heuristic(goal, neighbor)
                frontier.put(neighbor, priority)
                camefrom[neighbor] = current
        i += 1
    print ' Searches:', i
    return camefrom, costsofar


### Finally, after searching I need to build the path.
def ReconstructPath(camefrom, start, goal):
    current = goal
    path = [current]
    while current != start:
        current = camefrom[current]
        path.append(current)
    path.append(start) # optional
    path.reverse() # optional
    print ' Path length:', len(path)-3 # -1 for len() delta, then -2 because it also counts A,Z as spots.
    return path








### Define grid/graph/map features
TileWidth = 2
GridWidth = 30
GridHeight = 15

# Start, goal position
startx = 1
starty = 7
goalx = 20
goaly = 8

# Create WALLS only in straight lines.  [(,),(,)]
walls = [ [(3,1),(3,14)], 
          [(1,1),(29,1)], 
          #[(10,3),(10,15)],
          [(4,13),(8,13)],   
          [(5,3),(10,3)], 
          [(10,3),(10,5)],
          [(10,5),(13,5)],
          [(13,5),(13,14)],
          [(10,10),(10,14)],
          [(10,14),(14,14)],
          #[(23,0),(23,1)],
          [(15,10),(25,10)],
          #[(23,4),(23,8)],
          [(28,1),(28,14)],
          [(25,13),(28,13)],
 ]
Walls = []
for i in walls:
  x1 = i[0][0]
  y1 = i[0][1]
  x2 = i[1][0]
  y2 = i[1][1]
  if x2-x1 == 0:
    Walls.extend( [(x1,y) for y in range(y1,y2)] )
  if y2-y1 == 0:
    Walls.extend( [(x,y1) for x in range(x1,x2)] )

# Create WATER only in straight lines
waterweight = 10
water = [ [(15,2),(15,9)],
          [(16,2),(16,10)],
          [(17,2),(17,10)],
          [(18,5),(18,10)],
]
Water = []
for i in water:
  x1 = i[0][0]
  y1 = i[0][1]
  x2 = i[1][0]
  y2 = i[1][1]
  if x2-x1 == 0: # (x1 = x2)
    Water.extend( [(x1,y) for y in range(y1,y2)] )
  if y2-y1 == 0: # (y1 = y2)
    Water.extend( [(x,y1) for x in range(x1,x2)] )

# Create WEEDS only in straight lines
weedsweight = 5
weeds = [ [(22,9),(27,9)],
          [(22,8),(25,8)],
          [(22,7),(25,7)],
          [(23,6),(24,6)],
]
Weeds = []
for i in weeds:
  x1 = i[0][0]
  y1 = i[0][1]
  x2 = i[1][0]
  y2 = i[1][1]
  if x2-x1 == 0: # (x1 = x2)
    Weeds.extend( [(x1,y) for y in range(y1,y2)] )
  if y2-y1 == 0: # (y1 = y2)
    Weeds.extend( [(x,y1) for x in range(x1,x2)] )





### Create and draw grids (no weights and weights) with no searches
print ''
print ''
g = Grid(GridWidth, GridHeight)
g.walls = Walls
g.water = Water
g.weeds = Weeds
#DrawGrid(g, TileWidth)

gw = GridWithWeights(GridWidth, GridHeight)
gw.walls = Walls
gw.water = Water
gw.weeds = Weeds
gw.weights = {pos: weights for pos,weights in zip(Water+Weeds,[waterweight]*len(Water)+[weedsweight]*len(Weeds))}
DrawGrid(gw, TileWidth, start=(startx,starty), goal=(goalx, goaly))





### Draw Breadth First Search
print ''
print ''
print 'BREADTH FIRST'
CameFrom = BreadthFirst3(g, (startx, starty), (goalx, goaly))
DrawGrid(g, TileWidth, flow=CameFrom, start=(startx, starty), goal=(goalx, goaly))
DrawGrid(g, TileWidth, start=(startx,starty), goal=(goalx, goaly), path=ReconstructPath(CameFrom, start=(startx, starty), goal=(goalx, goaly)))


### Dijkstra Search
print ''
print ''
print 'DIJKSTRA'
#print ' Flow, Z-A' 
CameFrom, CostSoFar = Dijkstra(gw, (startx, starty), (goalx, goaly))
#DrawGrid(gw, TileWidth, flow=CameFrom, start=(startx, starty), goal=(goalx, goaly))
DrawGrid(gw, TileWidth, number=CostSoFar, start=(startx, starty), goal=(goalx, goaly))
DrawGrid(gw, TileWidth, start=(startx,starty), goal=(goalx, goaly), path=ReconstructPath(CameFrom, start=(startx, starty), goal=(goalx, goaly)))
print ''


### Greedy Best-first Search
print ''
print ''
print 'GREEDY BEST-FIRST'
CameFrom = GreedyFirst(gw, (startx, starty), (goalx, goaly))
DrawGrid(gw, TileWidth, flow=CameFrom, start=(startx, starty), goal=(goalx, goaly))
DrawGrid(gw, TileWidth, start=(startx,starty), goal=(goalx, goaly), path=ReconstructPath(CameFrom, start=(startx, starty), goal=(goalx, goaly)))
print ''


### A* Search
print ''
print ''
print 'A STAR'
#print ' Flow, Z-A'
CameFrom, CostSoFar = AStar(gw, (startx, starty), (goalx, goaly))
#DrawGrid(gw, TileWidth, flow=CameFrom, start=(startx, starty), goal=(goalx, goaly))
DrawGrid(gw, TileWidth, number=CostSoFar, start=(startx, starty), goal=(goalx, goaly))
DrawGrid(gw, TileWidth, start=(startx,starty), goal=(goalx, goaly), path=ReconstructPath(CameFrom, start=(startx, starty), goal=(goalx, goaly)))
print ''

### A* Search, no weights
'''
print ''
print ''
print 'A STAR, NO WEIGHTS'
gw = GridWithWeights(GridWidth, GridHeight)
gw.walls = Walls
gw.water = Water
gw.weeds = Weeds
CameFrom, CostSoFar = AStar(gw, (startx, starty), (goalx, goaly))
DrawGrid(gw, TileWidth, number=CostSoFar, start=(startx, starty), goal=(goalx, goaly))
DrawGrid(gw, TileWidth, start=(startx,starty), goal=(goalx, goaly), path=ReconstructPath(CameFrom, start=(startx, starty), goal=(goalx, goaly)))
print ''
'''
