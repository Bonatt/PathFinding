### From http://www.redblobgames.com/pathfinding/a-star/implementation.html for code source and other literature.
# Also see https://pythontips.com/2013/08/07/the-self-variable-in-python-explained/ for class, __init__, and self semiexplained.


import math
import collections
import random
import heapq



# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return math.sqrt(x)
# Redefine pi to something shorter
pi = math.pi
print ''




###### Visuals. What to draw where.

### Create graph with locations (x,y). Find locations inside map, outside impassable terrain, and neighbors that meet those criteria.
class Grid:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.walls = []
    
    def in_bounds(self, pos):
        (x, y) = pos
        return 0 <= x < self.width and 0 <= y < self.height
    
    def passable(self, pos):
        return pos not in self.walls
    
    def neighbors(self, pos):
        (x, y) = pos
        results = [(x+1, y), (x, y-1), (x-1, y), (x, y+1)]
        if (x + y) % 2 == 0: results.reverse() # aesthetics
        results = filter(self.in_bounds, results)
        results = filter(self.passable, results)
        return results

### Takes above, adds weight
class GridWithWeights(Grid):
    def __init__(self, width, height):
        #super(GridWithWeights,self).__init__(width, height)
        Grid.__init__(self, width, height)
        self.weights = {}
    def cost(self, from_node, to_node):
        return self.weights.get(to_node, 1)


### Create the graph STYLE for drawing in terminal.
def Tile(graph, wallwidth, pos, graphtype):
    if pos in graph.walls: tile = '#'*wallwidth
    elif 'start' in graphtype and pos == graphtype['start']: tile = 'A'
    elif 'goal' in graphtype and pos == graphtype['goal']: tile = 'Z'
    elif 'point' in graphtype and graphtype['point'].get(pos, None) is not None:
        (x1, y1) = pos
        (x2, y2) = graphtype['point'][pos]
        # Points to position it came from
        #'''
        if x2 == x1 + 1: tile = '>' #'\u2192'
        if x2 == x1 - 1: tile = '<' #'\u2190'
        if y2 == y1 + 1: tile = 'v' #'\u2193'
        if y2 == y1 - 1: tile = '^' #'\u2191'
        #'''
        # Points to position it went to (not quite)
        '''
        if x2 == x1 + 1: t = '<'
        if x2 == x1 - 1: t = '>'
        if y2 == y1 + 1: t = '^'
        if y2 == y1 - 1: t = 'v'
        '''
    elif 'number' in graphtype and pos in graphtype['number']: tile = '%d' % graphtype['number'][pos]
    elif 'path' in graphtype and pos in graphtype['path']: 
        tile = 'o'
        '''
        print graphtype
        print pos
        (x1, y1) = pos
        (x2, y2) = graphtype['path'][pos]
        # Points to position it went to
        if x2 == x1 + 1: t = '<'
        if x2 == x1 - 1: t = '>'
        if y2 == y1 + 1: t = '^'
        if y2 == y1 - 1: t = 'v'
        '''
    else: tile = '.'
    return tile


### Draw graph in terminal. Default wallwidth = 2
def DrawGrid(graph, wallwidth=2, **graphtype):
    for y in range(graph.height):
        for x in range(graph.width):
            print '%%-%ds' %wallwidth %Tile(graph, wallwidth, (x,y), graphtype),
            #print Tile(graph, (x,y), graphtype, wallwidth),
        print ''






###### The meaty searching and pathfinding algorithms.

### This 'Queue' class is just a wrapper around the built-in 'collections.deque' class.
class Queue:
    def __init__(self):
        self.elements = collections.deque()
    def empty(self):
        return len(self.elements) == 0
    def put(self, x):
        self.elements.append(x)
    def get(self):
        return self.elements.popleft()

### Above, with priority
class PriorityQueue:
    def __init__(self):
        self.elements = []
    def empty(self):
        return len(self.elements) == 0
    def put(self, item, priority):
        heapq.heappush(self.elements, (priority, item))
    def get(self):
        return heapq.heappop(self.elements)[1]



### Define Breadth-first Search. Only works with string example at bottom of page.
def BreadthFirst1(graph, start):
    # print out what we find
    frontier = Queue()
    frontier.put(start)
    visited = {}
    visited[start] = True
    
    while not frontier.empty():
        current = frontier.get()
        print('Visiting %r' % current)
        for neighbor in graph.neighbors(current):
            if neighbor not in visited:
                frontier.put(neighbor)
                visited[neighbor] = True


### To reconstruct paths, need to store location of where we came from, so I've renamed visited (True/False) to camefrom (location).
def BreadthFirst2(graph, start):
    # return "camefrom"
    frontier = Queue()
    frontier.put(start)
    camefrom = {}
    camefrom[start] = None
    
    while not frontier.empty():
        current = frontier.get()
        for neighbor in graph.neighbors(current):
            if neighbor not in camefrom:
                frontier.put(neighbor)
                camefrom[neighbor] = current
    
    return camefrom


### All we need to do is add an if statement in the main loop. This is optional for Breadth First Search or Dijkstra's Algorithm,
# but effectively required for Greedy Best-First Search and A*
def BreadthFirst3(graph, start, goal):
    frontier = Queue()
    frontier.put(start)
    camefrom = {}
    camefrom[start] = None
    
    while not frontier.empty():
        current = frontier.get()
        if current == goal:
            break
        for neighbor in graph.neighbors(current):
            if neighbor not in camefrom:
                frontier.put(neighbor)
                camefrom[neighbor] = current
    
    return camefrom





### Define Dijkstra Search
def Dijkstra(graph, start, goal):
    frontier = PriorityQueue()
    frontier.put(start, 0)
    camefrom = {}
    costsofar = {}
    camefrom[start] = None
    costsofar[start] = 0
    
    while not frontier.empty():
        current = frontier.get()
        if current == goal:
            break
        for neighbor in graph.neighbors(current):
            newcost = costsofar[current] + graph.cost(current, neighbor)
            if neighbor not in costsofar or newcost < costsofar[neighbor]:
                costsofar[neighbor] = newcost
                priority = newcost
                frontier.put(neighbor, priority)
                camefrom[neighbor] = current
    
    return camefrom, costsofar









### Both Greedy Best-First Search and A* use a heuristic function. 
# The only difference is that A* uses both the heuristic and the ordering from Dijkstra's Algorithm.
def heuristic(a, b):
    (x1, y1) = a
    (x2, y2) = b
    return abs(x1 - x2) + abs(y1 - y2)

def AStar(graph, start, goal):
    frontier = PriorityQueue()
    frontier.put(start, 0)
    camefrom = {}
    costsofar = {}
    camefrom[start] = None
    costsofar[start] = 0
    
    while not frontier.empty():
        current = frontier.get()
        if current == goal:
            break
        for neighbor in graph.neighbors(current):
            newcost = costsofar[current] + graph.cost(current, neighbor)
            if neighbor not in costsofar or newcost < costsofar[neighbor]:
                costsofar[neighbor] = newcost
                priority = newcost + heuristic(goal, neighbor)
                frontier.put(neighbor, priority)
                camefrom[neighbor] = current
    
    return camefrom, costsofar








### Finally, after searching I need to build the path.
def ReconstructPath(camefrom, start, goal):
    current = goal
    path = [current]
    while current != start:
        current = camefrom[current]
        path.append(current)
    path.append(start) # optional
    path.reverse() # optional
    print 'Path length:', len(path)-3 #-1, then -2 because it also counts A,Z as spots.
    return path









'''
# Utility function for dealing with square grid.
def from_id_width(id, width):
    return (id%width, id//width)
# data from main article
Walls1 = [from_id_width(id, width=30) for id in [21,22,51,52,81,82,93,94,111,112,123,124,133,134,141,142,153,154,163,164,171,172,173,174,175,183,184,193,194,201,202,203,204,205,213,214,223,224,243,244,253,254,273,274,283,284,303,304,313,314,333,334,343,344,373,374,403,404,433,434]]
'''

WallWidth = 3
GridWidth = 30
GridHeight = 15 #20

#Walls2 = [(i%GridWidth, i//GridWidth) for i in [21,22,51,52,81,82,93,94,111,112,123,124,133,134,141,142,153,154,163,164,171,172,173,174,175,183,184,193,194,201,202,203,204,205,213,214,223,224,243,244,253,254,273,274,283,284,303,304,313,314,333,334,343,344,373,374,403,404,433,434]]
Walls2 = [(21,0),(22,0),(21,1),(22,1),(21,2),(22,2),(3,3),(4,3),(21,3),(22,3),(3,4),(4,4),(13,4),(14,4),(21,4),(22,4),(3,5),(4,5),(13,5),(14,5),(21,5),(22,5),(23,5),(24,5),(25,5),(3,6),(4,6),(13,6),(14,6),(21,6),(22,6),(23,6),(24,6),(25,6),(3,7),(4,7),(13,7),(14,7),(3,8),(4,8),(13,8),(14,8),(3,9),(4,9),(13,9),(14,9),(3,10),(4,10),(13,10),(14,10),(3,11),(4,11),(13,11),(14,11),(13,12),(14,12),(13,13),(14,13),(13,14),(14,14)]

nWalls = int(GridWidth*GridHeight/3)
Walls3 = [(random.randint(0,GridWidth),random.randint(0,GridHeight)) for i in range(nWalls)]



### Draw grid, no searches
g = Grid(GridWidth, GridHeight)
g.walls = Walls2 #Walls3
DrawGrid(g, WallWidth)
print ''


### Draw Breadth First Search
startx = 8
starty = 7
'''
CameFrom = BreadthFirst2(g, (startx, starty))
DrawGrid(g, WallWidth, point=CameFrom, start=(startx, starty))
print ''
'''

goalx = 22
goaly = 12
CameFrom = BreadthFirst3(g, (startx, starty), (goalx, goaly))
print ''
print 'Breadth First, flow, Z-A:'
DrawGrid(g, WallWidth, point=CameFrom, start=(startx, starty), goal=(goalx, goaly))
print ''
print 'Breadth First, path:'
DrawGrid(g, WallWidth, path=ReconstructPath(CameFrom, start=(startx, starty), goal=(goalx, goaly)))







startx = 1
starty = 4
goalx = 7
goaly = 8

#gw = GridWithWeights(GridWidth, GridHeight)
#gw.walls = Walls2
gw = GridWithWeights(10, 10)
gw.walls = [(1, 7), (1, 8), (2, 7), (2, 8), (3, 7), (3, 8)]
gw.weights = {pos: 5 for pos in [(3, 4), (3, 5), (4, 1), (4, 2),
                                       (4, 3), (4, 4), (4, 5), (4, 6),
                                       (4, 7), (4, 8), (5, 1), (5, 2),
                                       (5, 3), (5, 4), (5, 5), (5, 6),
                                       (5, 7), (5, 8), (6, 2), (6, 3),
                                       (6, 4), (6, 5), (6, 6), (6, 7),
                                       (7, 3), (7, 4), (7, 5)]}

print ''
DrawGrid(gw, WallWidth)




### Dijkstra Search
print ''
print 'Dijkstra, flow, Z-A:' 
CameFrom, CostSoFar = Dijkstra(gw, (startx, starty), (goalx, goaly))
DrawGrid(gw, WallWidth, point=CameFrom, start=(startx, starty), goal=(goalx, goaly))
print''
print 'Dijkstra, cost:'
DrawGrid(gw, WallWidth, number=CostSoFar, start=(startx, starty), goal=(goalx, goaly))
print ''
print 'Dijkstra, path:'
DrawGrid(gw, WallWidth, path=ReconstructPath(CameFrom, start=(startx, starty), goal=(goalx, goaly)))
print ''









### A* Search
print ''
print 'A*, flow, Z-A:'
CameFrom, CostSoFar = AStar(gw, (startx, starty), (goalx, goaly))
DrawGrid(gw, WallWidth, point=CameFrom, start=(startx, starty), goal=(goalx, goaly))
print''
print 'A*, cost:'
DrawGrid(gw, WallWidth, number=CostSoFar, start=(startx, starty), goal=(goalx, goaly))
print ''
print 'A*, path:'
DrawGrid(gw, WallWidth, path=ReconstructPath(CameFrom, start=(startx, starty), goal=(goalx, goaly)))
print ''















'''

class GridWithWeights(Grid):
    def __init__(self, width, height):
        #super(Grid, self).__init__()#self, width, height)
        Grid.__init__(self,width,height)
        self.weights = {}
    def cost(self, from_node, to_node):
        return self.weights.get(to_node, 1)

gw = GridWithWeights(GridWidth, GridHeight)



From https://stackoverflow.com/questions/576169/understanding-python-super-with-init-methods

class Base(object):
    def __init__(self):
        print "Base created"

class ChildA(Base):
    def __init__(self):
        Base.__init__(self)

class ChildB(Base):
    def __init__(self):
        super(ChildB, self).__init__()

ChildA() 
ChildB()

'''











### Short example with a graph space consisting of letter nodes and uni- or bidirectional edges, or vectors, joining them.
'''
class SimpleGraph:
    def __init__(self):
        self.edges = {}

    def neighbors(self, id):
        return self.edges[id]

example_graph = SimpleGraph()
example_graph.edges = {'A': ['B'],
                       'B': ['A', 'C', 'D'],
                       'C': ['A'],
                       'D': ['E', 'A'],
                       'E': ['B']}

BreadthFirst_1(example_graph, 'A')
'''
